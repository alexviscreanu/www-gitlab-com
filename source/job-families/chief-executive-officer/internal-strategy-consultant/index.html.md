---
layout: job_family_page
title: Internal Strategy Consultant  
---

The Internal Strategy Consultant reports to the Chief of Staff and works to support the Office of the CEO.  Internal consultants can quickly gather and analyze all data related to a problem and present next steps. They are the internal strategy consultants of GitLab who move from problem to problem in different functional areas.

Please note this role is equivalent to the "Staff" level in terms of base salary and stock. The compensation calcualtor below intentionally only shows staff level compensation. 

## Responsibilities
- Fill in knowledge gaps of the Chief of Staff
- Assist in the creation of Decision-Making Frameworks for the Office of the CEO
- Identify procedural gaps in existing workflows and work to resolve with optimization, automation, and data
- Support the creation of prepared materials (documents, decks) for the Chief of Staff
- Translate practical needs into technical requirements
- Support special projects that are urgent and important
- Execute on projects for the Chief of Staff, as directed
- Demonstrate GitLab values in all work

## Requirements
- Detail-oriented forward thinker, familiar with the industry
- Excellent written, verbal, and technical communicator
- Experience making data-driven decisions
- Able to manage multiple tasks with competing timelines and deliverables
- Track record of leadership independent of role
- History of working cross-functionally to move projects forward
- Experience at a strategy consulting firm is preferred
- 2+ years experience in high growth startup environments

## Performance Indicators 
- Meetings shifted from CEO - hours spent in meetings decreases overtime by Chief of Staff stepping in, supported by Internal Consultants
- Throughput - Issues or merge requests closed as measurement of projects iterated on

## Specializations
Internal consultants will almost always have specializations that support the gaps in knowledge of the Chief of Staff

### Data
- Experience stepping into a new data source and preparing new analyses
- A familiarity with proxy metrics where actual measurements aren’t available 
- Ability to guide conversations related to strategic choices of performance indicators

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/)

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our globals recruiters
- Then, candidates will be invited to schedule a 50 minute interview with the Chief of Staff
- Next, candidates will be invited to schedule a number of 50 minute peer interviews
- Finally, candidates will meet with our CEO who will conduct the final interview

As always, the interviews and screening call will be conducted via a video call. See more details about our hiring process on the [hiring handbook](https://about.gitlab.com/handbook/hiring/).
