---
layout: markdown_page
title: "Category Vision - Container Scanning"
---

- TOC
{:toc}

## Description

### Overview

Container Scanning tests your Docker images against known vulnerabilities that may affect software that is installed in the image. It doesn't check the application code only, but it extends security to all the installed system components.

Users often use existing images as the base for their containers. It means that they rely on the security of those images and their preinstalled software. Unfortunately, this software is subject to vulnerabilities, and this may affect the security of the entire project.

### Goal

Our best practices are to package applications into containers, so they can deployed to Kubernetes.

Our goal is to provide Container Scanning as part of the standard development process. This means that Container Scanning is executed every time a new commit is pushed to a branch. We also include Container Scanning as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Container Scanning results are available in the merge request security report, where only new vulnerabilities, introduced by the new code, are shown. A full report is also available in the pipeline details page.

We want to make Container Scanning results available also in the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status.

Another place where Container Scanning results can be useful is the [GitLab Container Registry](https://docs.gitlab.com/ee/user/project/container_registry.html). Images built during pipelines are stored in the registry, and then used for deployments. Integrating Container Scanning into GitLab Container Registry will help to [monitor if it is safe to deploy a specific version of the app](https://gitlab.com/gitlab-org/gitlab-ee/issues/8790).

### Roadmap

- [First MVC (already shipped)](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Container Scanning improvements](https://gitlab.com/groups/gitlab-org/-/epics/299)

## What's Next & Why

We want Container Scanning to provide information about images deployed to the production environment.

The next MVC is to [add more information to reports](https://gitlab.com/gitlab-org/gitlab-ee/issues/5528)

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/530)

## Competitive Landscape

- [Black Duck](https://www.blackducksoftware.com/solutions/container-security)
- [Snyk](https://snyk.io/container-vulnerability-management)
- [Sonatype Nexus](https://www.sonatype.com/containers)
- [Qualys](https://www.qualys.com/apps/container-security/)

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. Since this is a relatively new scope for us, we must aim at being included in the next researches.

We can get valuable feedback from analysts, and use it to drive our vision.

- [Gartner](https://www.gartner.com/doc/3888664/container-security--image-analysis)
- [Forrester](https://www.forrester.com/report/Now+Tech+Container+Security+Q4+2018/-/E-RES142078)

GitLab also considers Container Scanning as part of Software Composition Analysis (SCA) as defined in our [Solutions](/handbook/product/categories/#solutions), since vulnerabilities for base images can be considered very similar to vulnerabilities for software dependencies.

## Top Customer Success/Sales Issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/6946)
- [Issue 2](https://gitlab.com/gitlab-org/gitlab-ce/issues/49562)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name%5B%5D=customer&label_name%5B%5D=container%20scanning)

## Top user issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/8790)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=popularity&label_name%5B%5D=container%20scanning)

## Top internal customer issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ce/issues/56668)

## Top Vision Item(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/8482)
- [Issue 2](https://gitlab.com/gitlab-org/gitlab-ee/issues/8790)
