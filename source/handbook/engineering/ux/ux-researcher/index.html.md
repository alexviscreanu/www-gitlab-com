---
layout: markdown_page
title: "UX Researcher Workflow"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## UX Researcher onboarding

If you are just starting out here at GitLab, welcome! Make sure to review all the pages here in the UX Research section of the handbook to help get you get oriented. There is also a specific page dedicated to [UX Researcher onboarding](/handbook/engineering/ux/uxresearcher-onboarding/).

### How we decide what to research

UX Researchers collaborate with Product Managers to determine the scope and priority of research studies. UX Researchers should sit in on monthly planning calls for their relevant product stages to stay informed about what Product feels are the most important initiatives at GitLab. Additionally, UX Researchers should proactively offer ways in which they can assist in the delivery of such initiatives.

### UX Researcher tools

All UX Researchers have individual accounts for the following tools:

[SurveyMonkey](https://www.surveymonkey.com/) - All UX Researchers should utilize SurveyMonkey for surveys. Please use the `GitLab Theme` to style your survey. The GitLab logo should be positioned in the top left hand corner of every page (applied automatically, resize to `small`).

[UsabilityHub](https://usabilityhub.com/) - Used to build design evaluations, such as first click tests, preference tests and five second tests. UsabilityHub should not be used for surveys.

[MailChimp](https://mailchimp.com) - Used to send campaigns to subscribers of GitLab First Look.

[OptimalWorkshop](https://www.optimalworkshop.com) - Used for card sorts and tree testing. We do not have an ongoing subscription to OptimalWorkshop. We purchase a monthly license as and when required.

[Zoom Pro Account](https://zoom.us/) - We use Zoom to run usability testing sessions and user interviews.

### Research methods

We use a wide variety of research methods that include (but are not limited to):

* Usability testing (led by Product Design with guidance from UX Research)
* User interviews
* Surveys
* Beta testing
* Design evaluations (led by Product Design with guidance from UX Research) 
    * First click tests
    * Preference tests 
    * Five second tests
* Competitor analysis
* Card sorts
* Tree tests
* [Buy a feature](https://www.innovationgames.com/buy-a-feature/)
* Diary studies

#### Reviewing a design evaluation (guidance for UX Researchers)

1. UX Researchers should familiarize themselves with the process outlined in [How to create a design evaluation (guidance for Product Designers)](/handbook/engineering/ux/ux-designer/index.html#creating-a-design-evaluation-guidance-for-product-designers).

1. When sending a design evaluation to GitLab First Look users, ensure standard processes are followed; for example, distributing the study to a test sample of users and sending the study to GitLab First Look subscribers who have opted into design evaluations from the relevant product stage.

#### Incentives

* User interview or usability testing: $60 (or equivalent currency) Amazon Gift Card per 30 minutes.
* Surveys or card sorts: Opportunity to win 1 of 3 $30 (or equivalent currency) Amazon Gift Cards.
* Beta testing: Unpaid.
* Design evaluations: Unpaid.

Amazon gift cards are country specific. When purchasing a gift card, ensure you use the appropriate Amazon store for a user's preferred country.

When attempting to send incentives internationally, your credit card or PayPal may be denied. You can offer to send an Amazon.com card (not in the user's country's store) or use [transferwise](https://transferwise.com/), which requires bank routing information.

### How we work

Like other departments at GitLab, UX Researchers follow the [GitLab Workflow](/handbook/communication/#everything-starts-with-a-merge-request) and the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline). 

We follow a monthly research cycle and use [milestones](https://docs.gitlab.com/ee/user/project/milestones/) to schedule our work. A typical monthly research cycle is as follows:

**8th of each month (or next business day)**
* Engineering/Product Kickoff
    
* UX Researchers begin working through their [UX Research checklist](/handbook/engineering/ux/ux-research/#checklist-templates).

**8th to 21st of each month.**
* UX Researchers undertake research.

**22nd of each month.**
* Release shipped.
    
* Product Managers and Product Designers start [Milestone Planning](/handbook/engineering/ux/ux-department-workflow/#milestone-planning) for the next release.

**22nd to the last day of the month.**
* UX Researchers analyze research and hold a wash-up meeting with relevant stakeholders.

**1st to 7th of each month.**
* UX Researchers document the study’s findings.
* UX Researchers begin preparing for research studies in the upcoming milestone (this might involve: gathering final requirements from stakeholders, drafting screeners, etc). 

Depending on the scope of each research study, a UX Researcher typically works on 1-3 studies per research cycle. Sometimes, larger studies may need to span more than 1 research cycle.

#### Scheduling issues

* UX Researchers assign themselves to issues. 

* When a new research proposal is raised in the [UX Research project](https://gitlab.com/gitlab-org/ux-research), you will be `@` mentioned to give your input. This doesn't mean that you have to start working on the issue right away. You should:

    * Quickly review the proposal, and assign the issue to yourself, if it relates to your product stage. 
    
    * Estimate the issue's priority by collaborating with the relevant Product Manager for the product stage.
    
    * Add a [milestone](https://docs.gitlab.com/ee/user/project/milestones/) to the issue. The milestone indicates when you plan to deliver the completed research to stakeholders. 
        
        For example: If you apply the milestone of `11.8` to an issue. Using the timescales outlined in [How we work](/handbook/engineering/ux/ux-research/#how-we-work). You will work on the research issue between 8th January to 7th February 2019. Ideally, you will discuss the results of the research with the relevant stakeholders between 22nd January to 31st January 2019 in order to assist with [Milestone Planning](/handbook/engineering/ux/ux-department-workflow/#milestone-planning).

* UX Researchers should aim to schedule 80% of their capacity to work on responsibilities outlined in the [role descriptions](/job-families/engineering/ux-researcher/) and try to block off the remaining 20% for other proactive work. This can be anything from exploring GitLab as a product to expanding your UX research skills by reading industry articles. 

#### Working on a research study

1. Update the `Research proposal` with the following:
    * Label the issue with the area of GitLab you’re testing (for example, `navigation`), the status of the issue (`in progress`) and the Product stage (for example, `manage`).
    * Add a milestone to the issue.
    * Mark the issue as `confidential` until the research is completed, so it doesn’t influence user behavior.
    * Assign the issue to yourself.
    * Add a checklist of actions that you plan to undertake. Next to each item, add an estimated deadline of when you plan to complete it. This makes it easier for people to understand where the research is up to.
    * Add related issue numbers.
1. Conduct the research. Ensure you keep the checklist up to date.
1. Document the study's findings within the [UXR_Insights repository](https://gitlab.com/gitlab-org/uxr_insights)
    * Refer to the repository's [ReadMe](https://gitlab.com/gitlab-org/uxr_insights/blob/master/README.md) for instructions on how to do this.
    * Use the repository's issue templates to guide you through the process of adding your findings.
1. Schedule a wash-up meeting.
    * Ensure you link to the study's Epic in the calendar invitation at least 24 hours prior to the meeting.
1. Record and facilitate the wash-up meeting.
1. Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description.
1. Update the `Research proposal` with the following:
    * Link to the Epic.
    * Unmark the issue within the UX research project as `confidential`. (In some cases the issue may need to remain confidential if sensitive information is shared. If you’re unsure of whether an issue should remain confidential, please check with Sarah O’Donnell `@sarahod`).
    * Update the status of the issue to `done`.
    * Close the issue. You should stay assigned to closed issues so it's obvious who completed the research.

### Qualtrics

##### Creating a survey

1. Select `Blank survey project`
1. Give your project a name.
1. Enter survey questions. On the right hand side of the screen you will find:
    * [Question types](https://www.qualtrics.com/support/survey-platform/survey-module/editing-questions/question-types-guide/question-types-overview/) and formatting (number of answer choices, positioning and so on).
    * [Validation](https://www.qualtrics.com/support/survey-platform/survey-module/editing-questions/validation/) (force respondents to answer a question or request that they consider answering the question before leaving the page).
    * Actions (such as [page breaks](https://www.qualtrics.com/support/survey-platform/survey-module/editing-questions/add-page-break/), [skip logic](https://www.qualtrics.com/support/survey-platform/survey-module/question-options/skip-logic/) and [display logic](https://www.qualtrics.com/support/survey-platform/survey-module/question-options/display-logic/)).
1. Update the `Look & Feel` of your survey.
    * Please refer to [Look & Feel settings](/handbook/engineering/ux/ux-researcher/index.html#look--feel-settings) for guidance.
1. Adjust your [Survey Options](https://www.qualtrics.com/support/survey-platform/survey-module/survey-options/survey-options-overview/) as appropriate.
1. Under the [Tools](https://www.qualtrics.com/support/survey-platform/survey-module/survey-tools/survey-tools-overview/) menu, you will find a host of useful features such as:
    * `Collaborate` - Allows you to share your survey with another Qualtrics user so you can edit it and/or analyze the results together.
    * `Spell check` - Supports English (US) only.
    * `Generate test responses` - Generate automated dummy responses for your survey to see what the dataset and report will look like before sending the survey to actual participants.
    * `Check survey accessibility` - Determine whether your survey is accessible for respondents who use screen readers, and receive suggestions for improving your survey’s accessibility.
    * `Analyze Survey` - Open [ExpertReview‘s](https://www.qualtrics.com/support/survey-platform/survey-module/survey-checker/quality-iq-functionality/) suggestions for your survey.
    * `Import/export survey` - From/to a QSF, TXT or DOC file.
1. When you have finished building your survey, `Preview` it to ensure that everything is working the way you expect it to.
1. Once you are ready to launch your survey, click `Publish`.

##### Look & Feel settings
The following settings should be applied to your survey:
1. Theme: Blank
1. Layout: Modern
1. General
    * Progress bar: Without Text
    * Progress bar position: Top
1. Style
    * Primary colour: #554488
    * Secondary colour: #554488
    * Font: Arial   
    * Foreground contrast: Medium
    * Question spacing: Compact
    * Question text: 14px bold
    * Answer text: 14px
1. Motion
    * Page transition: None
    * Additional check boxes: Unticked
1. Logo
    * GitLab First Look Logo (stored in UX Research library)
    * Style: Banner
    * Placement: Left
    * Max height: 70px
    * Mobile scaling: 66%
1. Background
    * Background type: Color
    * Background color: #ffffff
    * Foreground contrast: Medium
    * Questions container: Off


##### Distributing a survey to GitLab First Look
1. Select `Contacts` (top right menu).
1. Select `Lists` (far left menu).
1. Select `GitLab First Look`
1. Select `List Options`
1. From the dropdown menu, select `Create Sample From List`. 
1. Give your sample a name.
1. Select your sample size (the number of people who you ideally want to distribute your survey to).
    * It is advisable to send your survey to a small test sample of users to begin with, to ensure that you receive the kind of responses you are looking for. 
    * When you distribute your survey to a larger audience, to avoid contacting the same users who have already answered your survey as part of your test sample, click `Advanced Sampling Criteria`, ensure the sampling criteria statement reads: `All of the following are true`, change the `Contact Activity` filter to `Survey` and change the `Select survey` filter to the survey you have already distributed.
1. Click `Add Sampling Criteria`
1. Ensure the sampling criteria statement reads: `All of the following are true`
1. Select `Embedded data`, select an `Embedded Data field`, select an `Operator` and enter/select an `Embedded Data Value`.
    * For every sample you create, you must always add sampling criteria for `Stage groups` and `Types of research`. When a person signs up to GitLab First Look, they specify what they would like to receive studies about (stage groups) and what type of research they would like to take part in. It’s extremely important that when you contact users, you only email them in relation to their preferences.
    * Remember the embedded data for `Stage groups` and `Types of research` can contain multiple values (it’s rare that users sign-up to receive emails abouts one particular stage group or want to take part in one form of research). Therefore, your `Operator` will always be `Contains`.
    * For more information on what embedded data is available, please refer to the [embedded data](/handbook/engineering/ux/ux-researcher/index.html#embedded-data) section.
1. Click `New Condition` to add additional sampling criteria.
1. Click `Create` once you have finished entering your sampling criteria. 
1. A progress bar will appear, this indicates that Qualtrics is building your sample.
1. Once the sample is ready, click `Go to Sample` (Alternatively you can navigate to `Lists` and your new sample will be visible under `All Lists`. Click on your sample to access it).
1. The number of contacts in your sample will show on the left hand side of the screen. 
    * [What to do if your sample size is lower than expected](/handbook/engineering/ux/ux-researcher/index.html#what-to-do-if-your-sample-size-is-lower-than-expected)
1. If you are happy with the number of contacts in your sample, you are now ready to send your survey to users. To do this, go to `Projects`
1. Select the project/survey you want to distribute to users.
1. Click `Distributions`
1. Click `Compose Email`
1. In the `To` field, navigate to: `Group Library: UX Research` -> `GitLab First Look` -> `Samples` and select the sample you created earlier.
1. Update the `From` field to: `firstlook@gitlab.com`
1. Update the `From Name` to: `GitLab First Look`
1. The `Reply-To Email` can either be your own, personal GitLab email address or `firstlook@gitlab.com`
    * If you are planning to leave a survey running while you are on vacation, please use `firstlook@gitlab.com` as this will automatically forward to all UX Researchers who may be able to assist with any user queries in your absence.
1. Update the `When` field to the time you would like to send the email.
1. Enter your `subject line`. 
    * A standard subject line we use for campaigns is: `Quick, new research study available!` however, if you wish, you may experiment with this. Please ensure that you use a [subject line checker](https://www.subjectline.com/) to evaluate your subject line.
1. Delete the standard text/links which appear in the WYSIWYG text area.
1. Click `Load Message` next to the Message field.
1. Navigate to `Group Library: UX Research` and select the template you would like to use.
1. Select `Use Fixed Text` option from the `Load Message` dropdown after the template has loaded. This stops any changes you make to the template from being saved permanently.
1. Edit the message text as appropriate.
    * By default, the call to action always links to the current project/survey you are planning to send. As does the screening survey text link found in the body of templates for `Usability testing` and `User interviews`.
1. When you have finished building your email, click `Send Preview Email` and enter your GitLab email address. A copy of the email will be sent to you for review.
1. If you are happy with your email, click `Send`

##### Embedded data
When users sign-up to GitLab First Look, we automatically collect the following embedded data:
1. `Stage groups`
    * `1` - Manage
    * `2` - Plan
    * `3` - Create
    * `4` - Verify
    * `5` - Package
    * `6` - Release
    * `7` - Configure
    * `8` - Monitor
    * `9` - Secure
    * `10` - Defend
    * `11` - Enablement & Growth
1. `Types of research`
    * `Beta testing`
    * `Cards sorts`
    * `Design evaluations`
    * `Surveys`
    * `Usability testing`
    * `User interviews`
1. `GitLab user` - Whether or not the person is a GitLab user.
    * `Yes` - the person is a GitLab user.
    * `No` - the person is not a GitLab user.
1. `Job title` - What the person's job title is.
    * `Back-end Engineer/Developer`
    * `Designer`
    * `DevOps Engineer`
    * `Executive (VP of Engineering, CTO, CEO, etc)`
    * `Front-end Engineer/Developer`
    * `Full-stack Engineer/Developer`
    * `Infrastructure Engineer`
    * `Operations Engineer`
    * `Penetration Tester`
    * `Product Manager`
    * `Project Manager`
    * `Quality Assurance Engineer`
    * `Researcher`
    * `Security Analyst`
    * `Security Professional`
    * `Scientist`
    * `Site Reliability Engineer`
    * `Software Engineer/Developer`
    * `Student`
    * `Systems Administrator/Engineer`
    * `Unemployed`
    * `Other`
1. `Organisation size` - How many people work within the user's organisation? (Added August 2019)
    * `0-10 people`
    * `11-100 people`
    * `101-500 people`
    * `501-1000 people`
    * `1001-10,000 people`
    * `10,000+ people`
1. `Team size` - How many people work within the user's team (Added August 2019)
    * `0 - it's just me!`
    * `1-5 people`
    * `6-10 people`
    * `11-20 people`
    * `21-30 people`
    * `30+ people`
1. `SaaS (GitLab.com) package`
    * `Free`
    * `Bronze`
    * `Silver`
    * `Gold`
    * `I don't know`
1. `Self managed package`
    * `Core`
    * `Starter`
    * `Premium`
    * `Ultimate`
    * `I don't know`

* Something missing from this list? Let Sarah know and she will get it added!

* When using an `Embedded Data Value` Qualtrics doesn't automatically auto-complete the value as you begin typing it. The search functionality can also be haphazard. Therefore, ensure you type out the value in full, as it is displayed above.

* Embedded data fields used to be case-sensitive. For the vast majority of Qualtrics' users, embedded data is no longer case-sensitive, meaning “test” and “Test” would be treated as the same field. However, Qualtrics still advise matching cases as a best practice, as there are a small portion of accounts where this change has not been made.

* Not all Embedded Data Values were created at the point of GitLab's First Look creation. Therefore, we don't necessarily have completed values for every user. It's also worth noting that some values for users may change over time. For example, someone may change jobs which may impact their job title, organisation and team size. Therefore, there may be some discrepancies in the information we hold about users. We plan to reduce this risk by periodically asking members of GitLab First Look to check and, where necessary, update the information we have on file for them.


##### What to do if your sample size is lower than expected
There could be a couple of reasons why your sample number is lower than expected:
1. The contact frequency for GitLab First Look has been exhausted. By default, no member of GitLab First Look receives an email more than once a week or four times a month. If this is the case, you will need to either delay sending your survey (you may only need to delay sending your survey by a day - reach out to the UX Research Coordinator for confirmation) or source users outside of the GitLab First Look panel.
1. There aren’t enough GitLab First Look members that match your sampling criteria.
1. There’s an error in your sampling criteria.

* For options 2 and 3, navigate to your sample and click `List options`, select `Edit sample` to change your sampling criteria.


#### Wash-up meetings
After a research initiative, the UX Researcher schedules a "Wash-up Meeting" with all stakeholders to help communicate findings and collaboratively agree on a list of improvements/actions to take forward.

* At least 24 hours prior to the wash-up meeting, the UX Researcher should add a Google document containing the research study’s key findings to the meeting’s calendar invitation.

* To ensure that the meeting is an effective use of everybody’s time, make sure to familiarize yourself with the study’s key findings prior to the meeting, and come prepared with suggestions.

### Checklist templates

The following are examples of checklists that you may want to add to a `Research proposal`.

#### Usability Testing
* Schedule users. (Deadline:)
* Write script. (Deadline:)
* Test the script. Conduct 1 usability testing session. Edit the script if required. (Deadline:)
* Conduct remaining usability testing sessions. (Deadline:)
* Pay users. (Deadline:)
* Analyze videos. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### User Interviews
* Schedule users. (Deadline:)
* Write interview guide. (Deadline:)
* Conduct the interviews. (Deadline:)
* Pay users. (Deadline:)
* Analyze videos. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Surveys
* Write survey questions. (Deadline:)
* Import survey questions into SurveyMonkey. (Deadline:)
* Test survey logic. (Deadline:)
* Distribute survey to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the survey. (Deadline:)
* Distribute survey to remaining users. (Deadline:)
* Cleanse data and analyze survey results. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Design Evaluations
* Write instructions and/or questions.(Deadline:)
* Transfer instructions and/or questions into UsabilityHub. (Deadline:)
* Distribute study to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the study. (Deadline:)
* Distribute survey to remaining users. (Deadline:)
* Cleanse data and analyze responses. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Card Sorts
* Write questions.(Deadline:)
* Set-up the study in OptimalWorkshop.
* Transfer questions into OptimalWorkshop. (Deadline:)
* Distribute study to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the study. (Deadline:)
* Distribute study to remaining users. (Deadline:)
* Cleanse data and analyze responses. (Deadline:)
* Document findings within the UXR_Insights repository (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the Epic link to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Add the next steps/recommendations as agreed upon in the wash-up meeting to the Epic description. (Deadline:)
* Update the `Research proposal` issue. Link to the report. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)
