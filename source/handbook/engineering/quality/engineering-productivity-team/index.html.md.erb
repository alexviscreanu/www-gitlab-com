---
layout: markdown_page
title: "Engineering Productivity team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Engineers in this team are dedicated to our [Engineering Productivity](/handbook/engineering/quality#engineering-productivity) efforts.

## Areas of Responsibility

* Make metrics-driven suggestions to improve engineering processes, velocity, and throughput.
  * Build automated [measurements and dashboards](#engineering-productivity-team-metrics) to gain insights into
    engineering productivity and understand what is working and what is not.
  * Make suggestions for improvements, monitor the results and iterate.
  * See the [GitLab Insights] project.
* Build productivity tooling to help speed up overall Engineering.
  * Increase contributor and developer productivity by improving the development setup, workflow, processes, and tools.
  * Improve the ease of use of our [GDK (GitLab Development Kit)].
  * Improve [Review Apps] for GitLab development (GDK in the cloud).
  * Improve [GCK (GitLab Development Kit based on Docker Compose) in beta].
* Build automated tooling to speed up issue and merge request review and triage.
   * Automated issues and merge requests triage.
   * Automated triage package generation.
   * See the [GitLab Triage] and [GitLab triage operations] projects.
   * Ensure workflow and label hygiene in our system which feeds into our metrics dashboard.
* Build automated tools to ensure the consistency and quality of the codebase and merge request workflow.
  * Automated [merge request coaching](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/Dangerfile).
  * Automated [merge request code quality checks](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.rubocop.yml).
* Help with maintaining [GitLab Docs]
  * Review new enhancements.
  * Contribute to organization of Engineering documentation.

## Team Members

<%= direct_team(manager_role: 'Interim Quality Engineering Manager, Engineering Productivity') %>

## Project Management

The global issue board for the team can be found here: [Quality - Engineering Productivity Team](https://gitlab.com/groups/gitlab-org/-/boards/1333450)

## Projects

* [GitLab] (pipelines, Danger bot)
* [GitLab Insights]
* [GitLab Triage]
* [GitLab triage operations]
* [GDK (GitLab Development Kit)]
* [GCK (GitLab Development Kit based on Docker Compose) in beta]
* [GitLab Docs](https://gitlab.com/gitlab-org/gitlab-docs)

## Engineering productivity team metrics

The Engineering Productivity team creates metrics in the following sources to aid in operational reporting.

- [Periscope Quality KPIs](https://app.periscopedata.com/app/gitlab/516343/Quality-KPIs)
- [Periscope Engineering Sandbox](https://app.periscopedata.com/app/gitlab/496118/Engineering-Productivity-Sandbox)
- [GitLab-Org Native Insights](https://gitlab.com/groups/gitlab-org/-/insights)
- [Review Apps monitoring dashboard](https://app.google.stackdriver.com/dashboards/6798952013815386466?project=gitlab-review-apps)
  - [Failed deployment Tiller logs](https://console.cloud.google.com/logs/viewer?authuser=0&interval=P1D&project=gitlab-review-apps&minLogLevel=0&expandAll=false&timestamp=2019-10-11T03:56:06.077000000Z&customFacets=&limitCustomFacetWidth=true&resource=k8s_cluster&advancedFilter=resource.type%3D%22k8s_container%22%0AlogName%3D%22projects%2Fgitlab-review-apps%2Flogs%2Fstderr%22%0Aresource.labels.project_id%3D%22gitlab-review-apps%22%0Aresource.labels.location%3D%22us-central1-b%22%0Aresource.labels.cluster_name%3D%22review-apps-ee%22%0Aresource.labels.namespace_name%3D%22review-apps-ee%22%0Aresource.labels.pod_name:%22tiller-deploy%22%0AtextPayload:(%22%5Btiller%5D%22%20AND%20(%22warning:%20Upgrade%22%20OR%20%22warning:%20Release%22))&scrollTimestamp=2019-10-11T02:10:10.956872965Z&dateRangeStart=2019-10-10T03:58:45.648Z&dateRangeEnd=2019-10-11T03:58:45.648Z)
- [Deprecated Quality Dashboard](https://quality-dashboard.gitlap.com/groups/gitlab-org)

### Product MRs merged per Product team member per month

This metric captures how many Merge Requests are merged into customer facing projects divided by the number of product team members.

Customer facing projects are projects which would come with an installation of GitLab. The current list can be found with all the projects identified in the [`projects_part_of_product.csv`](https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product.csv) file in the [`gitlab-data/analytics`](https://gitlab.com/gitlab-data/analytics) project.

Product team member is defined as all non-VP or Director role in Development, UX, Quality, and Product Management.

#### Requesting new projects to be included

With this measure, there is a preference towards adding new projects into the existing product. In the event that a new GitLab project is included in the product that a customer would receive then please open an issue [on the quality team tasks project](https://gitlab.com/gitlab-org/quality/team-tasks/issues/new).

Within the issue, please add details on how this project is included with an Omnibus or Cloud Native installation of GitLab for consideration by the Engineering Productivity team. The team will evaluate the request and make any updates to charts and metrics in the sources listed above.

## Communication guidelines

The Engineering Productivity team will make changes which can create notification spikes or new behavior for
GitLab contributors. The team will follow these guidelines in the spirit of [GitLab's Internal Communication Guidelines](/handbook/communication/#internal-communication).

### Pipeline changes

For changes which dogfood new pipeline features or changes to the pipeline flow, the team will
communicate on the company call, add to the Engineering Week in Review, and [#development](https://gitlab.slack.com/messages/C02PF508L) in Slack.

For all other pipeline changes, the team will add to the Engineering Week in Review and
[#development](https://gitlab.slack.com/messages/C02PF508L) in Slack.

### Automated triage policies

Communication guidelines are documented on the [triage-ops handbook page](/handbook/engineering/quality/triage-operations/#communicate-early-and-broadly-about-expected-automation-impact)

[GitLab]: https://gitlab.com/gitlab-org/gitlab
[GitLab Insights]: https://gitlab.com/gitlab-org/gitlab-insights
[GitLab Triage]: https://gitlab.com/gitlab-org/gitlab-triage
[GitLab triage operations]: https://gitlab.com/gitlab-org/quality/triage-ops
[GDK (GitLab Development Kit)]: https://gitlab.com/gitlab-org/gitlab-development-kit
[GCK (GitLab Development Kit based on Docker Compose) in beta]: https://gitlab.com/gitlab-org/gitlab-compose-kit
[Review apps]: https://docs.gitlab.com/ee/development/testing_guide/review_apps.html
