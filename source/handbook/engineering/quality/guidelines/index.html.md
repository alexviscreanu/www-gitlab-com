---
layout: markdown_page
title: "Guidelines"
---

## On this page
{:.no_toc}

- TOC
{:toc}

#### Child Pages
##### [Debugging QA Test Failures](/handbook/engineering/quality/guidelines/debugging-qa-test-failures/)
##### [Tips and Tricks](/handbook/engineering/quality/guidelines/tips-and-tricks/)

## Overview

Guidelines are high-level directives on how we carry out operations and solve challenges in the Quality Engineering department.

## Submitting and Reviewing code

For test automation changes, it is crucial that every change is reviewed by at least one Senior Test Automation Engineer in the Quality team.

We are currently setting best practices and standards for Page Objects and REST API clients. Thus the first priority is to have test automation related changes reviewed and approved by the team.
For test automation only changes, the quality team alone is adequate to review and merge the changes.

## Test Automation & Planning

* **Test plans as collaborative design document**: Test Plans as documented in [Test Engineering](/handbook/engineering/quality/test-engineering/) are design documents that aim to flush out optimal test coverage.
It is expected that engineers in every cross-functional team take part in test plan discussions.
* **E2E test automation is a collective effort**: Test Automation Engineers should not be the sole responsible party that automates the End-to-end tests.
The goal is to have engineers contribute and own coverage for their teams.
* **We own test infrastructure**: Test infrastructure is under our ownership, we develop and maintain it with an emphasis on ease of use, ease of debugging, and orchestration ability.
* `Future` **Disable feature by default until E2E test merged**: If a feature is to be merged without a QA test,
it **must** be behind a feature flag (disabled by default) until a QA test is written and merged as well.

## Test Failures

* **Fix failing test in `master` first**: [Failing tests on `master` are treated as the highest priority](/handbook/engineering/workflow/#broken-master).
* **Flaky tests are quarantined until proven stable**: A flaky test is as bad as no tests or in some cases worse due to the effort required to fix or even re-write the test.
As soon as detected it is quarantined immediately to stabilize CI, and then fixed as soon as possible, and monitored until it is fixed.
* **Close issue when the test is moved out of quarantine**: Quarantine issues should not be closed unless tests are moved out of quarantine.
* **Quarantine issues should be assigned and scheduled**: To ensure that someone is owning the issue, it should be assigned with a milestone set.
* **Make relevant stage group aware**: When a test fails no matter the reason, an issue should be created and made known to the relevant product stage group as soon as possible.
In addition to notifying that a test in their domain fails, enlist help from the group as necessary.
* **Failure due to bug**: If a test failure is a result of a bug, link the failure to the bug issue. It should be fixed as soon as possible.
* **Everyone can fix a test, the responsibility falls on the last who worked on it**: Anyone can fix a failing/flaky test, but to ensure that a quarantined test isn't ignored,
the last engineer who worked on the test is responsible for taking it out of [quarantine](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/qa/README.md#quarantined-tests).

### Debugging Test Failures

See [Debugging QA Pipeline Test Failures](/handbook/engineering/quality/guidelines/debugging-qa-test-failures/)

### Priorities

Test failure priorities are defined as follow:

* ~P1: Tests that are needed to verify fundamental GitLab functionality.
* ~P2: Tests that deal with external integrations which may take a longer time to debug and fix.

## Building as part of GitLab

* **GitLab features first**: Where possible we will implement the tools that we use as GitLab features.
* **Build vs buy**: If there is a sense of urgency around an area we may consider buying/subscribing to a service to solve our Quality challenges in a timely manner.
This is where building as part of GitLab is not immediately viable. An issue will be created to document the decision making process in our [team task](https://gitlab.com/gitlab-org/quality/team-tasks) issue tracker.
This shall follow our [dogfooding](/handbook/engineering#dogfooding) process.

## Test Failure Management Rotation

This is a schedule to share the responsibility of debugging/analysing the failures in the daily runs in:
* [Nightly pipeline](https://gitlab.com/gitlab-org/quality/nightly/pipelines): Tests are run [daily at 4:00 am UTC](https://gitlab.com/gitlab-org/quality/nightly/pipeline_schedules/9530/edit) and results are reported on the [#qa-nightly](https://gitlab.slack.com/messages/CGLMP1G7M) slack channel.
* [Staging pipeline](https://gitlab.com/gitlab-org/quality/staging/pipelines): Tests are run [daily at 4:00 am UTC](https://gitlab.com/gitlab-org/quality/staging/pipeline_schedules/9514/edit) and results are reported on the [#qa-staging](https://gitlab.slack.com/messages/CBS3YKMGD) slack channel.

Additional tests are also run as part of the release process:
* [Deployer pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/pipelines): This will also report into [#qa-staging](https://gitlab.slack.com/messages/CBS3YKMGD) and [#qa-production](https://gitlab.slack.com/messages/CCNNKFP8B). Reporting will be enabled as part of some [upcoming changes to deployment automation](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/201).

Please refer to the Quality team guidelines on [debugging QA pipeline test failures](/handbook/engineering/quality/guidelines/debugging-qa-test-failures/) for specific instructions on how to do an appropriate level of investigation and determine next steps for the failing test.

### Responsibility
* During the scheduled dates, the support tasks related to the test runs become the Directly Responsible Individual's ([DRI](/handbook/people-group/directly-responsible-individuals/)'s) highest priority.
* At the end of the schedule, the DRI should write a simple handoff note specifying the highlights of that week and post it in the [#quality](https://gitlab.slack.com/messages/C3JJET4Q6) slack channel and in the ["Quality Engineering weekly" meeting document](https://docs.google.com/document/d/12IcXnAkFcEIfi9EDZFaBcpIa8PrKIw7EL5KgSOeSTqw/edit?usp=sharing).

### Schedule

**September 2019 | October 2019 | November 2019 | December 2019**

| **Start Date** | **DRI**                                                     | **Secondary**                                               |
|----------------|-------------------------------------------------------------|-------------------------------------------------------------|
| 2019-09-16     | [Dan Davison](/company/team/#sircapsalot)                   | [Aleksandr Soborov](/company/team/#asoborov)                |
| 2019-09-23     | [Aleksandr Soborov](/company/team/#asoborov)                | [Sanad Liaquat](/company/team/#sanadliaquat)                |
| 2019-09-30     | [Sanad Liaquat](/company/team/#sanadliaquat)                | [Tomislav Nikić](/company/team/#asktomislav)                |
| 2019-10-07     | [Tomislav Nikić](/company/team/#asktomislav)                | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) |
| 2019-10-14     | [Walmyr Lima e Silva Filho](/company/team/#walmyrlimaesilv) | [Grant Young](/company/team/#grantyoung)                    |
| 2019-10-21     | [Grant Young](/company/team/#grantyoung)                    | [Nailia Iskhakova](/company/team/#niskhakova)               |
| 2019-10-28     | [Nailia Iskhakova](/company/team/#niskhakova)               | [Mark Lapierre](/company/team/#mdlap)                       |
| 2019-11-04     | [Mark Lapierre](/company/team/#mdlap)                       | [Jennie Louie](/company/team/#jennielouie)                  |
| 2019-11-11     | [Jennie Louie](/company/team/#jennielouie)                  | [Zeff Morgan](/company/team/#zeffer)                        |
| 2019-11-18     | [Zeff Morgan](/company/team/#zeffer)                        | [Désirée Chevalier](/company/team/#dchevalier2)             |
| 2019-11-25     | [Désirée Chevalier](/company/team/#dchevalier2)             | [Dan Davison](/company/team/#sircapsalot)                   |
| 2019-12-02     | [Dan Davison](/company/team/#sircapsalot)                   | [Aleksandr Soborov](/company/team/#asoborov)                |
| 2019-12-09     | [Aleksandr Soborov](/company/team/#asoborov)                | [Sanad Liaquat](/company/team/#sanadliaquat)                |
| 2019-12-16     | [Sanad Liaquat](/company/team/#sanadliaquat)                | [Tomislav Nikić](/company/team/#asktomislav)                |
| 2019-12-23     | [Ramya Authappan](/company/team/#at.ramya)                  | [Tanya Pazitny](/company/team/#tpazitny)                    |
| 2019-12-30     | [Tanya Pazitny](/company/team/#tpazitny)                    | [Ramya Authappan](/company/team/#at.ramya)                  |

### Responsibilities of the DRI and Secondary for scheduled pipelines
* The DRI does the [triage](/handbook/engineering/quality/guidelines/debugging-qa-test-failures/#steps-for-debugging-qa-pipeline-test-failures) and they let the counterpart TAE know of the failure.
* The DRI makes the call whether to fix or quarantine the test.
* The fix/quarantine MR should be reviewed by either the Secondary or the counterpart TAE (based on whoever is available). If both of them are not available immediately, then any other TAE can review the MR.  In any case, both the Secondary and the counterpart TAE are always CC-ed in all communications.
* The DRI should periodically take a look at the list of unassigned quarantined issues in [staging](https://gitlab.com/gitlab-org/quality/staging/issues) and in [nightly](https://gitlab.com/gitlab-org/quality/nightly/issues) and work on them.
* If the [DRI](/handbook/people-group/directly-responsible-individuals/) is not available during their scheduled dates (for more than 2 days), they can swap their schedule with another team member. If the DRI's unavailability during schedule is less than 2 days, the Secondary can help with the tasks.

### Responsibilities of the DRI and Secondary for deployment pipelines
* The DRI helps the delivery team debug test failures that are affecting the release process.
* The Secondary fills in when the DRI is not available.
