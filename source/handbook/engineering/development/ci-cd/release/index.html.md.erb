---
layout: markdown_page
title: "Release Team"
---

# Release Team

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

For an understanding of what this team is going to be working on take a look at [the product
vision](/direction/release/).

## Mission

The Release Team is focused on all the functionality with respect to
Continuous Delivery and Release Automation.

This team maps to [Release](/handbook/product/categories/#release) devops stage.

## Team Members

The following people are permanent members of the Release Team:

<%= direct_team(manager_role: 'Engineering Manager, Release (CD)') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Release/, direct_manager_role: 'Engineering Manager, Release (CD)') %>

## Technologies

Like most GitLab backend teams, we spend a lot of time working in Rails on the main [GitLab CE app](https://gitlab.com/gitlab-org/gitlab-ce), but we also do a lot of work in Go which is used heavily in [GitLab Pages](https://gitlab.com/gitlab-org/gitlab-pages). Familiarity with Docker and Kubernetes is also useful on our team.

## Common Links

 * [Issue Tracker](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Arelease)
 * [Slack Channel](https://gitlab.slack.com/archives/s_release)
 * [Roadmap](/direction/release/)

## How to work with us

### On issues

Issues that contribute to the release stage of the devops toolchain have the `~"devops::release"` label.

### In Slack

The team's primary Slack channel is `#s_release`, and the public managers channel is `#release-managers`. We also support a number of feature channels for discussons or questions about a specific [feature area](/handbook/product/categories/#release-stage). We no longer support issue specific channels, as they are easy to lose track of and fragment the discussion. Supported channels are:

* Continuous delivery: `f_continuous_delivery`
* Release orchestration: `f_release_orchestration`
* Release governance: `f_release_governance`
* Incremental rollout: `f_incremental_rollout`
* Review apps: `f_review_apps`
* Feature flags: `f_feature_flags`
* Merge trains: `f_merge_trains`
* Secrets management: `f_secrets_management`
* Pages: `gitlab-pages`

### Team Workflow

We use the [Release Team Workflow board](https://gitlab.com/groups/gitlab-org/-/boards/1285883?&label_name[]=devops%3A%3Arelease) to plan and track features as they make their way from idea to production. This board details the various stages an issue can exist in as it is being developed. It's not necessary that an issue go through all of these stages, and it is allowed for issues to move back and forward through the workflow as they are iterated on.

#### Workflow Stages

Below is a description of each stage, its meaning, and any requirements for that stage.

* `workflow::start`
  * The entry point for workflow scoped labels. From here, Issues will either move through the validation workflow, go directly to planning and scheduling, or in certain cases, go directly to the development steps.
* `workflow::problem validation`
  * This stage aims to produce a [clear and shared understanding of the customer problem](/handbook/product-development-flow/#validation-phase-2-problem-validation).
* `workflow::solution validation`
  * This output of this stage is a [clear prototype of the solution to be created](/handbook/product-development-flow/#validation-phase-3-solution-validation).
* `workflow::ready for development`
  * This stage indicates the issue is ready for engineering to begin their work.
  * Issues in this stage must have a `UX Ready` label, as well as either `frontend`, `backend` or both labels to indicate which areas will need focus.
* `workflow::in dev`
  * This stage indicates that the issue is actively being worked on by one or more developers.
* `workflow::in review`
  * This stage indicates that the issue is undergoing code review by the development team and/or undergoing design review by the UX team.
* `workflow::verification`
  * This stage indicates that everything has been merged and the issue is waiting for verification after a deploy.


### Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate various status updates. There are currently 3 status updates configured in Geekbot, one is weekly and two are daily:

#### Weekly Status Update
The **Weekly Status Update** is configured to run at noon on Fridays, and contains three questions:

1. ***What progress was made on your deliverables this week?*** (MRs and demos are good for this)

    The goal with this question is to show off the work you did, even if it's only part of a feature. This could be a whole feature, a small part of a larger feature, an API to be used later, or even just a copy change.

2. ***What do you plan to work on next week?*** (think about what you'll be able to merge by the end of the week)

    Think about what the next most important thing is to work on, and think about what part of that you can accomplish in one week. If your priorities aren't clear, talk to your manager.

3. ***Who will you need help from to accomplish your plan for next week?*** (tag specific individuals so they know ahead of time)

    This helps to ensure that the others on the team know you'll need their help and will surface any issues earlier.

#### Daily Standup

The **Daily Standup** is configured to run each morning Monday through Thursday and posts to `#g_release` Slack channel. It has just one question:

1. ***Is there anything you could use a hand with today, or any blockers?***

    This check-in is optional and can be skipped if you don't need any help or don't have any blockers. Be sure to ask for help early, your team is always happy to lend a hand.

#### Daily Social

The optional **Daily Social** is configured to run each morning and posts to #g_cicd_social. It has just one question:

1. ***What was something awesome, fun, or interesting that you did yesterday outside of work?***

    This check-in is optional and can be skipped if you don't have anything to share.
