---
layout: markdown_page
title: "Recruiting Process - Sourcer Tasks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiting Process Framework - Sourcer Tasks
{: #framework-source}

### Step 8/S: Source top talent, schedule screens

The purpose of sourcing is to generate a large list of qualified potential 
candidates. From this list, Recruiting will solicit interest, and we hope to 
increase the number of qualified candidates in our pipeline.

### Step 8.1: Recruiter: Ask for Sourcing Support

The Recruiter/Recruiting Manager/Hiring Leader can request sourcing support for 
a particular role. If so, they will reach out to the Sourcing Manager and the Sourcer
aligned to the team/location of the vacancy to request sourcing.
Please refer to the [Recruiting Alingmnent](/handbook/hiring/recruiting-alignment/)
page for further details.

### Step 8.2: Recruiter: Schedule and complete Intake

As a common practice, the Recruiter will schedule an intake call. Sourcer should
come prepared for the intake session with insights and required questions to 
serve as a talent advisor. For example:
* Specific markets or companies to target (they can refer to our [Sourcing Knowledge Base](https://docs.google.com/spreadsheets/d/1psTAn6vMxRKZVlQUv6CYxvbLmzSKNFi0k9ryCFNhUIY/edit#gid=290516232) during the intake)
* Location/time zone preferences if there are any
* Short-listed profiles etc.

Refer to [Step 5/R: Schedule intake](/handbook/hiring/recruiting-framework/recruiter/#step-5r-schedule-intake) and [Step 6/R: Complete Intake](/handbook/hiring/recruiting-framework/recruiter/#step-6r-complete-intake) in recruiting 
framework for detailed information on how to schedule and complete intake. 

### Step 8.3: Sourcer: Source candidates 

Sourcer uses multiple tools to source candidates with LinkedIn being the most 
used and efficient.  Ensure you have a LinkedIn Recruiter account and your 
ContactOut (email finder plugin) is installed.  

For additional details/information refer to our [Sourcing Handbook page.](/handbook/hiring/sourcing/)

### Step 8.4: Sourcer: Reach Out

Sourcers are responsible for reaching out to candidates. They can use our 
[reach out templates](https://docs.google.com/presentation/d/1ySqgLoYnFUGtb7hdywav6iSb_NBPRhfIs6WZlGne6Ww/edit?usp=sharing) or create their own messaging for reaching out to candidates. 

### Step 8.5: Sourcer: Add to Greenhouse

Greenhouse is the only source of truth. Ensure your LinkedIn account is 
connected to Greenhouse to export candidates in one click as prospects 
([instructions](https://support.greenhouse.io/hc/en-us/articles/115005678103-Enable-LinkedIn-Recruiter-System-Connect-RSC-)). 
All approached candidates should be added in Greenhouse as a prospect. 

### Step 8.6: Sourcer: Use Greenhouse for tracking your prospects

We recommend to Sourcers to use the *Follow* button to get timely updates on 
candidate's status. As per the interview process, schedule the candidates for 
next steps (leave a note in Greenhouse to recruiter and CES team member with an 
update). For Example:- 
* [Schedule a screening call](/handbook/hiring/greenhouse/#scheduling-interviews-with-greenhouse) with candidate through Greenhouse or using recruiters’s calendly link. 
* Send Assessment through Greenhouse
* Send for review to recruiter or hiring manager
