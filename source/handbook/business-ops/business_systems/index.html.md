---
layout: markdown_page
title: "Business System Analysts"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Business Systems Analyst)

### Links
*  [Job Description](/job-families/finance/business-system-analyst/)
*  [Epic Board: Business Operations](https://gitlab.com/groups/gitlab-com/business-ops/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=BSA)

### General Ongoing
*  Involvement with evaluating tools: We can provide templates for vendor "Request for Proposals" and user stories; we can help review your user stories

### Current High Level Work
Karlia Kue
*  Scale Onboarding
*  Offboarding compliance
*  Company Tool Stack: Inventory, Information

Jamie Carey
*  Portal Analysis: system integrations, GitLabber user stories, identifying pain points of all departments
*  Scale Access Requests + Access Request Role Templates (Working with IT Ops/Help)


