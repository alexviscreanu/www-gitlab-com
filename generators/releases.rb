require 'date'
require 'yaml'

# Generates the Markdown used by the `/releases/` page based on monthly
# release blog posts in this repository
class ReleaseList
  # Don't generate for versions prior to 8.0
  # CUTOFF = Date.new(2015, 9, 22)
  # Don't generate for versions prior to 9.2
  # 9.2 is the first relase post with YAML features
  CUTOFF = Date.new(2017, 5, 22)

  def count(number = StringIO.new)
    # We started release posts with YAML features in 9.2 on May 22nd, 2017.
    # There were 49 releases prior to that on the 22nd until 5.0 on March 22nd, 2013. See - https://about.gitlab.com/community/mvp/
    # There were 5 releases prior to that on or around the 22nd dating back to 3.0.0 release on October 22nd, 2012. See - https://gitlab.com/gitlab-com/www-gitlab-com/issues/5396#note_224693712
    number.puts(release_posts.count + 49 + 5).to_s
    number.string if number.respond_to?(:string)
  end

  def generate(output = StringIO.new)
    release_posts.each do |post|
      output.puts "## [GitLab #{post.version}](#{post.relative_url})"
      output.puts post.date.to_time.strftime('%A, %B %e, %Y').to_s
      output.puts '{: .date}'
      output.puts

      ultimate_features = []
      premium_features = []
      starter_features = []
      core_features = []

      post.highlights.each do |highlight|
        if highlight.tier[0] == "ultimate"
          ultimate_features.push(highlight)
        elsif highlight.tier[0] == "premium"
          premium_features.push(highlight)
        elsif highlight.tier[0] == "starter"
          starter_features.push(highlight)
        else
          core_features.push(highlight)
        end
      end

      output.puts "### Ultimate"
      ultimate_features.each do |highlight|
        output.puts "- #{highlight}"
      end
      output.puts

      output.puts "### Premium"
      premium_features.each do |highlight|
        output.puts "- #{highlight}"
      end
      output.puts

      output.puts "### Starter"
      starter_features.each do |highlight|
        output.puts "- #{highlight}"
      end
      output.puts

      output.puts "### Core"
      core_features.each do |highlight|
        output.puts "- #{highlight}"
      end
      output.puts
    end

    # Return the final string if `output` supports it
    output.string if output.respond_to?(:string)
  end

  # Returns an Array of monthly release posts in descending order
  def release_posts
    root = File.expand_path('../source/posts', __dir__)

    # find's `-regex` option is too dumb to do what we want, so use grep too
    find = %(find "#{root}" -type f -iname "*-released.html.md")
    grep = %(grep #{grep_flags} '\\d{4}-\\d{2}-22-gitlab-\\d{1,2}-\\d{1,2}-released')
    sort = %q(sort -n)

    `#{find} | #{grep} | #{sort}`
      .lines
      .map    { |path| ReleasePost.new(path) }
      .reject { |post| post.date < CUTOFF }
      .reverse
  end

  private

  def grep_flags
    # GNU supports PCRE via `-P`; for others (i.e., BSD), we want `-E`
    if `grep -V`.include?('GNU grep')
      '-P'
    else
      '-E'
    end
  end

  class ReleasePost
    attr_reader :filename, :title, :date, :version

    def initialize(filename)
      @filename = filename.strip

      extract_attributes
    end

    def relative_url
      format('/%<year>d/%<month>0.2d/%<day>0.2d/%<title>s', year: date.year, month: date.month, day: date.day, title: title)
    end

    # Returns an Array of "highlights"
    #
    # If a data file exists for the release post, we extract the feature list
    # from its YAML. If not, we read the entire release post and consider
    # anything with a level two Markdown header - `##` - a highlighted feature.
    #
    # Finally, the list is filtered to reject excluded headers, such as the
    # Other Improvements, that are in every release post.
    def highlights
      return @highlights if @highlights

      @highlights =
        if data_file?
          highlights_from_data_file
        else
          highlights_from_post
        end

      @highlights.reject!(&:excluded?)

      @highlights
    end

    private

    class Highlight
      attr_reader :title, :link, :tier

      # These headers are in every post and should not be considered highlights
      EXCLUSIONS = [
        'other changes',
        'upgrade barometer', # deprecated on GitLab 11.8
        'important notes on upgrading',
        'installation',
        'updating',
        'enterprise edition',
        'deprecations',
        'other improvements',
        "this month's most valuable person"
      ].freeze

      def initialize(title, link = nil, tier = nil)
        @title = title
        @link  = link
        @tier = tier
      end

      def excluded?
        EXCLUSIONS.any? do |exclusion|
          title.downcase.start_with?(exclusion)
        end
      end

      def link?
        link.to_s.strip != ''
      end

      def to_s
        if link?
          "[#{title}](#{link})"
        else
          title
        end
      end
    end

    def extract_attributes
      match = filename.match(
        /
          (?<date>\d{4}-\d{2}-\d{2})
          -
          (?<title>
            gitlab-
            (?<major>\d{1,2})-(?<minor>\d{1,2})
            -released
          )
        /xi
      )

      @title   = match['title']
      @date    = Date.parse(match['date'])
      @version = "#{match['major']}.#{match['minor']}"
    end

    def data_file?
      File.exist?(data_file_path)
    end

    def data_file_path
      filename = File.basename(@filename, '.html.md').tr('-', '_')
      File.expand_path("../data/release_posts/#{filename}.yml", __dir__)
    end

    def highlights_from_post
      File
        .read(filename)
        .lines
        .select { |l| l =~ /\A##[^#]/ }
        .map    { |l| l.sub(/\A##([^#]+)\z/, '\1').strip }
        .map    { |l| remove_editions(l) }
        .map    { |l| Highlight.new(l) }
    end

    def highlights_from_data_file
      features = YAML.safe_load(File.read(data_file_path)).fetch('features', {})

      # def collect_features(name, link, tier, url)
      #   link ||= url
      #   Highlight.new(name, link, tier)
      # end

      features
        .values
        .flatten
        .collect do |f|
          link = f['documentation_link'] || f['performance_url']
          Highlight.new(f['name'], link, f['available_in'])
        end
    end

    def remove_editions(line)
      line
        .sub(/ eep\z/, '')
        .sub(/ ees\z/, '')
        .sub(/ ce\z/, '')
    end
  end
end
