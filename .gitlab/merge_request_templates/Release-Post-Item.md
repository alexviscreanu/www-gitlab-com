<!-- SET THE RIGHT LABELS AND MILESTONE (let the autocomplete guide you) -->

/label ~"release post" ~"Technical Writing" ~"devops::
/milestone %"
/assign `@PM`

Engineer(s): `@engineers` | Product Marketing: `@PMM` | Tech Writer: `@techwriter`

### Content checklist

* [ ] Description is informative and focuses on the problem we're solving and the value we're delivering
* [ ] Check feature and product names are in capital case
* [ ] Check headings are in sentence case
* [ ] Check Feature availability (Core, Starter, Premium, Ultimate badges)
* [ ] Make sure feature description is positive and cheerful
* [ ] Ensure all text is consistently hard wrapped at a suitable column boundary
* [ ] Screenshot/video is included (optional for secondary items)
* [ ] Check all images size < 300KB [compressed](https://tinypng.com/)
* [ ] Check if the image shadow is applied correctly. Add `image_noshadow: true` when an image already has a shadow
* [ ] Ensure videos and iframes are wrapped in `<figure class="video_container">` tags (for responsiveness)
* [ ] Check documentation link points to the latest docs (`documentation_link`)
* [ ] Check that documentation is updated and easy to understand
* [ ] Check that all links to about.gitlab.com content are relative URLs
* [ ] Run the content through an automated spelling and grammar check
* [ ] Validate all links are functional and have [meaningful text](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) for SEO (e.g., "click here" is bad link text)
* [ ] Remove any remaining instructions (comments)
* [ ] Feature is added to [`data/features.yml`](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) (with accompanying screenshots)

### Review

When the above is complete and the content is ready for review, it should be reviewed by Product Marketing and Tech Writing.
Please assign the PMM and Tech writer to the MR when it is ready to be reviewed!

* [ ]  PMM review
* [ ]  Tech writing

### References

* Issue:
* Release post (optional):
